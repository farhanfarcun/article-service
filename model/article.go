package model

import (
	"fmt"
	"time"
)

// Article store an article
type Article struct {
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
	Category  []Category
}

type Category string

const (
	Politic       Category = "politic"
	News          Category = "news"
	Internet      Category = "internet"
	Entertainment Category = "entertainment"
)

// CreateArticle create an article
func CreateArticle(name, slug, body string, categories ...Category) (Article, error) {
	//WHEN
	if len(categories) == 0 {
		return Article{}, fmt.Errorf("Category needed")
	}
	for _, category := range categories {
		switch category {
		case Politic, News, Internet, Entertainment:
		default:
			return Article{}, fmt.Errorf("Category not valid")
		}
	}

	//THEN
	return Article{
		Name:      name,
		Slug:      slug,
		Body:      body,
		CreatedAt: time.Now(),
		Category:  categories,
	}, nil
}
