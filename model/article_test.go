package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanCreateArticleWithOneCategory(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{Politic}

	// When
	got, err := CreateArticle(name, slug, body, category...)

	// Then
	assert.Equal(t, name, got.Name)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Equal(t, category, got.Category)
	assert.NoError(t, err)
}

func TestCantCreateArticleIfCategoryInvalid(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{"budaya"}

	// When
	_, err := CreateArticle(name, slug, body, category...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Category not valid", err.Error())
}

func TestCanCreateArticleWithCategories(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{Politic, News, Entertainment}

	// When
	got, err := CreateArticle(name, slug, body, category...)

	// Then
	assert.Equal(t, name, got.Name)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Equal(t, category, got.Category)
	assert.NoError(t, err)
}

func TestCantCreateArticleWithoutCategory(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"

	// When
	_, err := CreateArticle(name, slug, body)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Category needed", err.Error())
}
